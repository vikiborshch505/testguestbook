<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Message;
use Illuminate\Http\Response;
use Illuminate\View\View;

// Validator
use Illuminate\Support\Facades\Validator;

//
class MessageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $data = array(
            'title' => 'Гостевая книга',
            'metaTitlePrefix' => 'Обзор сообщений. ',
            'countOfMessages' => Message::count(),
            'allMessages' => Message::latest()->paginate(3),
        );
        return view('index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * /
     * public function create()
     * {
     * //
     * }
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $input = $request->all();
        // Проверяем полученные данные
        $validationResult = $this->_validation($input);

        // Если данные прошли проверку, тогда в $validationResult будет NULL.
        if (!is_null($validationResult)) {
            // Если данные не прошли проверку, тогда в $validationResult находится заполненый Response с адресом "редиректа" и ошибками.
            return $validationResult;
        } // if
        // Создаём новый объект Message
        $message = new Message();
        // Заполняем объект Message данными
        $message->name = $input['name'];
        $message->message = $input['message'];

        // Сохраняем новые данные в базе данных
        if ($message->save()) {
            return redirect()
                ->route('messages')
                ->with('sessionMessage', 'Запись добавлена.');
        } // if
        // Если в процессе записи нового объекта произойдёт ошибка, отобразим ошибку 500.
        abort(500);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Message $message
     * @return Application|Factory|Response|View
     */
    public function show(Message $message)
    {
        // Поиск ID предыдущей записи
        $previus = Message::where('id', '<', $message->id)
        ->select('id')
        ->orderby('id', 'desc')
        ->first();
        $previusID = ($previus != NULL) ? $previus->id : NULL; // Проверка. У первой записи нет предыдущей.

        // Поиск ID следующей записи
        $next = Message
        ::where('id', '>', $message->id)
        ->select('id')
        ->orderby('id', 'asc')
        ->first();
        $nextID = ($next != NULL) ? $next->id : NULL;


        $data = array(
            'title' => 'Гостевая книга .<br> Просмотр сообщения.',
            'metaTitlePrefix' => 'Просмотр одного сообщения. ',
            'message' => $message,
            'previusID' => $previusID,
            'nextID' => $nextID
        );

        return view('show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Message $message
     * @return Application|Factory|Response|View
     */
    public function edit(Message $message)
    {
        // объединяем данные в один массив, чтобы их передать шаблонам.
        $data = array(
            'title' => 'Исправление сообщения.',
            'metaTitlePrefix' => 'Исправление сообщения. ',
            'message' => $message,
        );
        // Обрабатываем шаблоны и заканчиваем работу
        return view('edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Message $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Message $message)
    {
        // Из всего $request сохраняем в виде архива только те данные, которые мы послали через формуляр.
        $input = $request->all();
        // Проверяем полученные данные
        $validationResult = $this->_validation($input, $message->id);

        // Если данные прошли проверку, тогда в $validationResult будет NULL.
        if (!is_null($validationResult)) {
            // Если данные не прошли проверку, тогда в $validationResult находится заполненый Response с адресом "редиректа" и ошибками.
            return $validationResult;
        } // if
        // Заполняем объект Message данными
        $message->name = $input['name'];
        $message->message = $input['message'];

        // Сохраняем новые данные в базе данных
        if ($message->save()) {
            return redirect()
                ->route('messages', array('#' . $message->id))
                ->with('sessionMessage', 'Запись изменена.');
        } // if
        // Если в процессе записи нового объекта произойдёт ошибка, отобразим ошибку 500.
        abort(500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Message $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Message $message)
    {
        $message->delete();
        return redirect()
            ->route('messages')
            ->with('sessionMessage', 'Запись удалена.');
    }

    /**
     * Проверяет данные, которые были введены в форму.$message
     *
     * Если данные не проходят валидацию, то в выходной массив добавляется поле 'response'.
     *
     * @param array $input
     * @param int $id - ID записи, которую мы меняем. Если не задан, то считается, что мы создаём новую запись.
     * @return Application|\Illuminate\Http\RedirectResponse|Response|\Illuminate\Routing\Redirector
     */
    private function _validation($input, $id = NULL)
    {

        $validatorErrorMessages = array(
            'required' => 'Поле :attribute обязательно к заполнению',
        );

        // Проверяем данные
        $validator = Validator::make(
            $input, // Данные, которые мы получили из формы
            array(// Описываем требования к данным
                'name' => 'required|max:255',
                'message' => 'required',
            ),
            $validatorErrorMessages); // Подключаем словарь с возможными ошибками.
        // Определяем прошла ли проверка удачно или нет.
        if ($validator->fails()) {
            // Проверка провалилась.
            // Теперь генерируем куда переадресовать страничку. Если $id задан, то мы обновляем уже существующие данные. Если же $id не задан, то мы создаём новую запись.
            $redirectURL = ($id == NULL) ?
                route('messages.index') :
                route('messages.edit', $id);

            // Подготавливаем "редирект"
            return redirect($redirectURL) // Куда
            ->withErrors($validator) // Сообщения об ошибках
            ->withInput(); // Введённые данные
        } // if
        // Проверка прошла удачно.
        return NULL;
    }

}
